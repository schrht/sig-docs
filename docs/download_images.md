# Downloading images produced by the Automotive SIG

The Automotive SIG provides OSBuild manifests and instructions to build AArch64 or x86_64 images
for systems, virtual machines, or a Raspberry Pi 4. If you would like to view our images, follow
the instructions on the [Building Images](https://sigs.centos.org/automotive/building/) page.

In an effort to continuously deliver working images, we are developing a process to automatically build and test images as we update them and as CentOS Stream changes. This is a work in progress. However, we can provide you with these [sample images](https://autosd.sig.centos.org/AutoSD-9/latest/sample-images/).
